
from humano import humano

if __name__ == "__main__":
    # Instanciar clase
    edad_elisa = 30
    nombre_elisa = "elisa"

    elisa = humano(edad_elisa, nombre_elisa)


    # Llamar atributos
    #print("{} tiene {} años".format(elisa.nombre, elisa.edad))

    #print("viewEdad Elisa: {}".format(elisa.viewEdad()))

    edad_pedro = 40
    nombre_pedro = "Pedro"
    pedro = humano(edad_pedro, nombre_pedro)

    #print("{} tiene {} años".format(pedro.nombre, pedro.edad))

    print(elisa.info_humano())