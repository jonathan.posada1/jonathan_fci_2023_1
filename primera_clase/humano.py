class humano():
    # Constructor
    def __init__(self, edad, nombre):

        # Atributos
        self.edad = edad
        self.nombre = nombre
        #print("name_class: {}".format(__name__))

        # Método
    def viewEdad(self):
        a = self.edad
        return a
    
    def viewName(self):
        return self.nombre
    
    def info_humano(self):
        return "{} tiene {} años".format(self.viewName(), self.viewEdad())